En encabezado del cartucho inicia el el octeto 100h

00000100  00 c3 50 01 ce ed 66 66  cc 0d 00 0b 03 73 00 83  |..P...ff.....s..|
00000110  00 0c 00 0d 00 08 11 1f  88 89 00 0e dc cc 6e e6  |..............n.|
00000120  dd dd d9 99 bb bb 67 63  6e 0e ec cc dd dc 99 9f  |......gcn.......|
00000130  bb b9 33 3e 50 4f 4b 45  4d 4f 4e 20 52 45 44 00  |..3>POKEMON RED.|
00000140  00 00 00 00 30 31 03 13  05 03 01 33 00 20 91 e6  |....01.....3. ..|

El control se pasa a la dirección 100h

0100	00		NOP
0101	C3 50 01	JP 0150h

El control nos manda a la 0150h

00000150  fe 11 28 03 af 18 02 3e  00 ea 1a cf c3 54 1f 3e  |..(....>.....T.>|
00000160  20 0e 00 e0 00 f0 00 f0  00 f0 00 f0 00 f0 00 f0  | ...............|
00000170  00 2f e6 0f cb 37 47 3e  10 e0 00 f0 00 f0 00 f0  |./...7G>........|
00000180  00 f0 00 f0 00 f0 00 f0  00 f0 00 f0 00 f0 00 2f  |.............../|
00000190  e6 0f b0 e0 f8 3e 30 e0  00 c9 f0 b8 f5 3e 03 e0  |.....>0......>..|

0150	FE 11		CP 11h
0152	28 03		JR Z, +3
0154	AF		XOR A		
0155	18 02		JR +2		
0157	3E 00		LD A, 00h	
0159	EA 1A CF	LD (CF1Ah), A 	
015C	C3 54 1F	JP 1F54h	; no hay forma de que se cumpla.

; 015Fh 
; Rutina de lectura de los botones.
; el resultado se coloca en la dirección: FFF8h
;
; bit 0 - A button
; bit 1 - B button
; bit 2 - Select button
; bit 3 - Start button
; bit 4 - Right
; bit 5 - Left
; bit 6 - Up
; bit 7 - Down
;
015F	3E 20		LD A, 20h		; lectura del pad.
0161	0E 00		LD C, 00h
0163	E0 00		LDH (FF00h+00h),A
0165	F0 00		LDH A,(FF00h+00h)
0167	F0 00		LDH A,(FF00h+00h)
0169	F0 00		LDH A,(FF00h+00h)
016B	F0 00		LDH A,(FF00h+00h)
016D	F0 00		LDH A,(FF00h+00h)
016F	F0 00		LDH A,(FF00h+00h)
0171	2F		CPL		; complemento de A
0172	E6 0F		AND 0Fh
0174	CB 37		SWAP A
0176	47		LD B, A
0177	3E 10		LD A, 10h		; lectura del pad.
0179	0E 00		LD C, 00h
017B	F0 00		LDH A,(FF00h+00h)
017D	F0 00		LDH A,(FF00h+00h)
017F	F0 00		LDH A,(FF00h+00h)
0181	F0 00		LDH A,(FF00h+00h)
0183	F0 00		LDH A,(FF00h+00h)
0185	F0 00		LDH A,(FF00h+00h)
0187	F0 00		LDH A,(FF00h+00h)
0189	F0 00		LDH A,(FF00h+00h)
018B	F0 00		LDH A,(FF00h+00h)
018D	F0 00		LDH A,(FF00h+00h)
018F	2F		CPL
0190	E6 0F		AND 0Fh
0192	B0		OR B
0193	E0 F8		LDH (FF00h+F8h),A	; almacenamiento temporal en fff8h
0195	3E 30		LD A, 30h		; lectura del pad.
0197	E0 00		LDH (FF00h+00h),A	
0199	C9		RET	




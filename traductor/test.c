#include <stdio.h>

void main(void)
{
  int i=0;
  unsigned char data=0;

  FILE *fp = fopen("test.data","w");

  for(i=0;i<256;i++)
  {
    data = 0xfe;
    fwrite(&data,1,1,fp);
    data = i;
    fwrite(&data,1,1,fp);
  }
}

#pragma once

/*
  Cambia el apuntador FILE *fp,
  a la dirección 'direccion'
  con un desplazamiento de bloque*16Ko.
*/
int cambiar_a_direccion(FILE *fp, unsigned short int bloque, unsigned short int direccion);

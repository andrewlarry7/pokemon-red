/*
  Traductor de binario para gameboy a códigos mnemónicos (Ensamblador)
*/
%{
#include <stdio.h>

#include "ensamblador_gb.h"

int i=0; 
int direccion_actual = 0x0000;
int banco_actual = 0;


/*
    Crear una dirección a partir de los dos caracteres que lee flex.

    Entrada: un apuntador a los dos caracters, 
		NOTA: como es yytext, se podría omitir??

    Salida: un entero (16 bits) con la dirección

    

    Este proceso es necesario porque las direcciones en el código 
    están almacenadas en formato 'littleendian'. 

*/
unsigned short int texto_a_direccion(char *yytext)
{
 unsigned char parte_baja = *(yytext);
 unsigned char parte_alta = *(yytext+1);

 unsigned short int direccion = 0;

 direccion = (unsigned short int)parte_alta;

 direccion <<=8;
 
 direccion |= parte_baja;

 return direccion;
}


void imprimir_direccion(int avance) {

  if( direccion_actual > 0x3FFF && banco_actual == 0 )
  {
    banco_actual ++;
  }
  if( direccion_actual > 0x7FFF )
  {
    banco_actual ++;
    direccion_actual = 0x4000;
  }

  printf("; %0xh (banco %02d) \n", direccion_actual, banco_actual);
  direccion_actual += avance; 
 }

%}

OCTETO .|\n
DIRECCION {OCTETO}{OCTETO}

%%
 /* 
   Instrucciones que se pueden traducir de forma directa y secuencial 

   Todas tienen una longitud de 1 octeto (byte).
   */

\x00 { printf("\tNOP"); imprimir_direccion(1);}

\x02 { printf("\tLD (BC),A"); imprimir_direccion(1); }
\x03 { printf("\tINC BC"); imprimir_direccion(1); }
\x04 { printf("\tINC B"); imprimir_direccion(1); }
\x05 { printf("\tDEC B"); imprimir_direccion(1); }
\x07 { printf("\tRLCA"); imprimir_direccion(1); }
\x09 { printf("\tADD HL,BC"); imprimir_direccion(1); }
\x0a { printf("\tLD A,(BC)"); imprimir_direccion(1); }
\x0b { printf("\tDEC BC"); imprimir_direccion(1); }
\x0c { printf("\tINC C"); imprimir_direccion(1); }
\x0d { printf("\tDEC C"); imprimir_direccion(1); }
\x0f { printf("\tRRCA"); imprimir_direccion(1); }

\x12 { printf("\tLD (DE),A"); imprimir_direccion(1); }
\x13 { printf("\tINC DE"); imprimir_direccion(1); }
\x14 { printf("\tINC D"); imprimir_direccion(1); }
\x15 { printf("\tDEC D"); imprimir_direccion(1); }
\x17 { printf("\tRLA"); imprimir_direccion(1); }
\x19 { printf("\tADD HL,DE"); imprimir_direccion(1); }
\x1a { printf("\tLD A,(DE)"); imprimir_direccion(1); }
\x1b { printf("\tDEC DE"); imprimir_direccion(1); }
\x1c { printf("\tINC E"); imprimir_direccion(1); }
\x1d { printf("\tDEC E"); imprimir_direccion(1); }
\x1f { printf("\tRRA"); imprimir_direccion(1); }
\x22 { printf("\tLD (HL+),A"); imprimir_direccion(1); }
\x23 { printf("\tINC HL"); imprimir_direccion(1); }
\x24 { printf("\tINC H"); imprimir_direccion(1); }
\x25 { printf("\tDEC H"); imprimir_direccion(1); }
\x27 { printf("\tDAA"); imprimir_direccion(1); }
\x29 { printf("\tADD HL,HL"); imprimir_direccion(1); }
\x2a { printf("\tLD A,(HL+)"); imprimir_direccion(1); }
\x2b { printf("\tDEC HL"); imprimir_direccion(1); }
\x2c { printf("\tINC L"); imprimir_direccion(1); }
\x2d { printf("\tDEC L"); imprimir_direccion(1); }
\x2f { printf("\tCPL"); imprimir_direccion(1); }
\x32 { printf("\tLD (HL-),A"); imprimir_direccion(1); }
\x33 { printf("\tINC SP"); imprimir_direccion(1); }
\x34 { printf("\tINC (HL)"); imprimir_direccion(1); }
\x35 { printf("\tDEC (HL)"); imprimir_direccion(1); }
\x37 { printf("\tSCF"); imprimir_direccion(1); }
\x39 { printf("\tADD HL,SP"); imprimir_direccion(1); }
\x3a { printf("\tLD A,(HL-)"); imprimir_direccion(1); }
\x3b { printf("\tDEC SP"); imprimir_direccion(1); }
\x3c { printf("\tINC A"); imprimir_direccion(1); }
\x3d { printf("\tDEC A"); imprimir_direccion(1); }
\x3f { printf("\tCCF"); imprimir_direccion(1); }

\x40 { printf("\tLD B,B"); imprimir_direccion(1); }
\x41 { printf("\tLD B,C"); imprimir_direccion(1); }
\x42 { printf("\tLD B,D"); imprimir_direccion(1); }
\x43 { printf("\tLD B,E"); imprimir_direccion(1); }
\x44 { printf("\tLD B,H"); imprimir_direccion(1); }
\x45 { printf("\tLD B,L"); imprimir_direccion(1); }
\x46 { printf("\tLD B,(HL)"); imprimir_direccion(1); }
\x47 { printf("\tLD B,A"); imprimir_direccion(1); }
\x48 { printf("\tLD C,B"); imprimir_direccion(1); }
\x49 { printf("\tLD C,C"); imprimir_direccion(1); }
\x4a { printf("\tLD C,D"); imprimir_direccion(1); }
\x4b { printf("\tLD C,E"); imprimir_direccion(1); }
\x4c { printf("\tLD C,H"); imprimir_direccion(1); }
\x4d { printf("\tLD C,L"); imprimir_direccion(1); }
\x4e { printf("\tLD C,(HL)"); imprimir_direccion(1); }
\x4f { printf("\tLD C,A"); imprimir_direccion(1); }

\x50 { printf("\tLD D,B"); imprimir_direccion(1); }
\x51 { printf("\tLD D,C"); imprimir_direccion(1); }
\x52 { printf("\tLD D,D"); imprimir_direccion(1); }
\x53 { printf("\tLD D,E"); imprimir_direccion(1); }
\x54 { printf("\tLD D,H"); imprimir_direccion(1); }
\x55 { printf("\tLD D,L"); imprimir_direccion(1); }
\x56 { printf("\tLD D,(HL)"); imprimir_direccion(1); }
\x57 { printf("\tLD D,A"); imprimir_direccion(1); }
\x58 { printf("\tLD E,B"); imprimir_direccion(1); }
\x59 { printf("\tLD E,C"); imprimir_direccion(1); }
\x5a { printf("\tLD E,D"); imprimir_direccion(1); }
\x5b { printf("\tLD E,E"); imprimir_direccion(1); }
\x5c { printf("\tLD E,H"); imprimir_direccion(1); }
\x5d { printf("\tLD E,L"); imprimir_direccion(1); }
\x5e { printf("\tLD E,(HL)"); imprimir_direccion(1); }
\x5f { printf("\tLD E,A"); imprimir_direccion(1); }

\x60 { printf("\tLD H,B"); imprimir_direccion(1); }
\x61 { printf("\tLD H,C"); imprimir_direccion(1); }
\x62 { printf("\tLD H,D"); imprimir_direccion(1); }
\x63 { printf("\tLD H,E"); imprimir_direccion(1); }
\x64 { printf("\tLD H,H"); imprimir_direccion(1); }
\x65 { printf("\tLD H,L"); imprimir_direccion(1); }
\x66 { printf("\tLD H,(HL)"); imprimir_direccion(1); }
\x67 { printf("\tLD H,A"); imprimir_direccion(1); }
\x68 { printf("\tLD L,B"); imprimir_direccion(1); }
\x69 { printf("\tLD L,C"); imprimir_direccion(1); }
\x6a { printf("\tLD L,D"); imprimir_direccion(1); }
\x6b { printf("\tLD L,E"); imprimir_direccion(1); }
\x6c { printf("\tLD L,H"); imprimir_direccion(1); }
\x6d { printf("\tLD L,L"); imprimir_direccion(1); }
\x6e { printf("\tLD L,(HL)"); imprimir_direccion(1); }
\x6f { printf("\tLD L,A"); imprimir_direccion(1); }

\x70 { printf("\tLD (HL),B"); imprimir_direccion(1); }
\x71 { printf("\tLD (HL),C"); imprimir_direccion(1); }
\x72 { printf("\tLD (HL),D"); imprimir_direccion(1); }
\x73 { printf("\tLD (HL),E"); imprimir_direccion(1); }
\x74 { printf("\tLD (HL),H"); imprimir_direccion(1); }
\x75 { printf("\tLD (HL),L"); imprimir_direccion(1); }
\x76 { printf("\tHALT"); imprimir_direccion(1); }
\x77 { printf("\tLD (HL),A"); imprimir_direccion(1); }
\x78 { printf("\tLD A,B"); imprimir_direccion(1); }
\x79 { printf("\tLD A,C"); imprimir_direccion(1); }
\x7a { printf("\tLD A,D"); imprimir_direccion(1); }
\x7b { printf("\tLD A,E"); imprimir_direccion(1); }
\x7c { printf("\tLD A,H"); imprimir_direccion(1); }
\x7d { printf("\tLD A,L"); imprimir_direccion(1); }
\x7e { printf("\tLD A,(HL)"); imprimir_direccion(1); }
\x7f { printf("\tLD A,A"); imprimir_direccion(1); }

\x80 { printf("\tADD A,B"); imprimir_direccion(1); }
\x81 { printf("\tADD A,C"); imprimir_direccion(1); }
\x82 { printf("\tADD A,D"); imprimir_direccion(1); }
\x83 { printf("\tADD A,E"); imprimir_direccion(1); }
\x84 { printf("\tADD A,H"); imprimir_direccion(1); }
\x85 { printf("\tADD A,L"); imprimir_direccion(1); }
\x86 { printf("\tADD A,(HL)"); imprimir_direccion(1); }
\x87 { printf("\tADD A,A"); imprimir_direccion(1); }
\x88 { printf("\tADC A,B"); imprimir_direccion(1); }
\x89 { printf("\tADC A,C"); imprimir_direccion(1); }
\x8a { printf("\tADC A,D"); imprimir_direccion(1); }
\x8b { printf("\tADC A,E"); imprimir_direccion(1); }
\x8c { printf("\tADC A,H"); imprimir_direccion(1); }
\x8d { printf("\tADC A,L"); imprimir_direccion(1); }
\x8e { printf("\tADC A,(HL)"); imprimir_direccion(1); }
\x8f { printf("\tADC A,A"); imprimir_direccion(1); }

\x90 { printf("\tSUB B"); imprimir_direccion(1); }
\x91 { printf("\tSUB C"); imprimir_direccion(1); }
\x92 { printf("\tSUB D"); imprimir_direccion(1); }
\x93 { printf("\tSUB E"); imprimir_direccion(1); }
\x94 { printf("\tSUB H"); imprimir_direccion(1); }
\x95 { printf("\tSUB L"); imprimir_direccion(1); }
\x96 { printf("\tSUB (HL)"); imprimir_direccion(1); }
\x97 { printf("\tSUB A"); imprimir_direccion(1); }
\x98 { printf("\tSBC A,B"); imprimir_direccion(1); }
\x99 { printf("\tSBC A,C"); imprimir_direccion(1); }
\x9a { printf("\tSBC A,D"); imprimir_direccion(1); }
\x9b { printf("\tSBC A,E"); imprimir_direccion(1); }
\x9c { printf("\tSBC A,H"); imprimir_direccion(1); }
\x9d { printf("\tSBC A,L"); imprimir_direccion(1); }
\x9e { printf("\tSBC A,(HL)"); imprimir_direccion(1); }
\x9f { printf("\tSBC A,A"); imprimir_direccion(1); }

\xa0 { printf("\tAND B"); imprimir_direccion(1); }
\xa1 { printf("\tAND C"); imprimir_direccion(1); }
\xa2 { printf("\tAND D"); imprimir_direccion(1); }
\xa3 { printf("\tAND E"); imprimir_direccion(1); }
\xa4 { printf("\tAND H"); imprimir_direccion(1); }
\xa5 { printf("\tAND L"); imprimir_direccion(1); }
\xa6 { printf("\tAND (HL)"); imprimir_direccion(1); }
\xa7 { printf("\tAND A"); imprimir_direccion(1); }
\xa8 { printf("\tXOR B"); imprimir_direccion(1); }
\xa9 { printf("\tXOR C"); imprimir_direccion(1); }
\xaa { printf("\tXOR D"); imprimir_direccion(1); }
\xab { printf("\tXOR E"); imprimir_direccion(1); }
\xac { printf("\tXOR H"); imprimir_direccion(1); }
\xad { printf("\tXOR L"); imprimir_direccion(1); }
\xae { printf("\tXOR (HL)"); imprimir_direccion(1); }
\xaf { printf("\tXOR A"); imprimir_direccion(1); }

\xb0 { printf("\tOR B"); imprimir_direccion(1); }
\xb1 { printf("\tOR C"); imprimir_direccion(1); }
\xb2 { printf("\tOR D"); imprimir_direccion(1); }
\xb6 { printf("\tOR (HL)"); imprimir_direccion(1); }
\xb7 { printf("\tOR A"); imprimir_direccion(1); }
\xb8 { printf("\tCP B"); imprimir_direccion(1); }
\xb9 { printf("\tCP C"); imprimir_direccion(1); }
\xba { printf("\tCP D"); imprimir_direccion(1); }
\xbb { printf("\tCP E"); imprimir_direccion(1); }
\xbc { printf("\tCP H"); imprimir_direccion(1); }
\xbd { printf("\tCP L"); imprimir_direccion(1); }
\xbe { printf("\tCP (HL)"); imprimir_direccion(1); }
\xbf { printf("\tCP A"); imprimir_direccion(1); }

\xc0 { printf("\tRET NZ"); imprimir_direccion(1); }
\xc1 { printf("\tPOP BC"); imprimir_direccion(1); }
\xc5 { printf("\tPUSH BC"); imprimir_direccion(1); }
\xc7 { printf("\tRST 00H"); imprimir_direccion(1); }
\xc8 { printf("\tRET Z"); imprimir_direccion(1); }
\xc9 { printf("\tRET"); imprimir_direccion(1); }
\xcf { printf("\tRST 08H"); imprimir_direccion(1); }
\xd0 { printf("\tRET NC"); imprimir_direccion(1); }
\xd1 { printf("\tPOP DE"); imprimir_direccion(1); }
\xd3 { printf("\t;"); imprimir_direccion(1); }
    /* \xcb { printf("\tPREFIX CB"); imprimir_direccion(1); } */

\xd5 { printf("\tPUSH DE"); imprimir_direccion(1); }
\xd7 { printf("\tRST 10H"); imprimir_direccion(1); }
\xd8 { printf("\tRET C"); imprimir_direccion(1); }
\xd9 { printf("\tRETI"); imprimir_direccion(1); }
\xdb { printf("\t; OPCODE DB "); imprimir_direccion(1); }
\xdd { printf("\t; OPCODE DD "); imprimir_direccion(1); }
\xdf { printf("\tRST 18H"); imprimir_direccion(1); }

\xe1 { printf("\tPOP HL"); imprimir_direccion(1); }
\xe2 { printf("\tLD (C),A"); imprimir_direccion(1);  }
\xe3 { printf("\tOPCODE E3;"); imprimir_direccion(1); }
\xe4 { printf("\tOPCODE E4;"); imprimir_direccion(1); }
\xe5 { printf("\tPUSH HL"); imprimir_direccion(1); }
\xe7 { printf("\tRST 20H"); imprimir_direccion(1); }
\xe9 { printf("\tJP (HL)"); imprimir_direccion(1); }
\xeb { printf("\t; OPCODE EB"); imprimir_direccion(1); }
\xec { printf("\t; OPCODE EC"); imprimir_direccion(1); }
\xed { printf("\t; OPCODE ED"); imprimir_direccion(1); }
\xef { printf("\tRST 28H"); imprimir_direccion(1); }

\xf1 { printf("\tPOP AF"); imprimir_direccion(1); }
\xf2 { printf("\tLD A,(C)"); imprimir_direccion(1);  }
\xf3 { printf("\tDI"); imprimir_direccion(1); }
\xf4 { printf("\t; OPCODE F4 "); imprimir_direccion(1); }
\xf5 { printf("\tPUSH AF"); imprimir_direccion(1); }
\xf7 { printf("\tRST 30H"); imprimir_direccion(1); }
\xf9 { printf("\tLD SP,HL"); imprimir_direccion(1); }
\xfb { printf("\tEI"); imprimir_direccion(1); }
\xfc { printf("\t; OPCODE FC"); imprimir_direccion(1); }
\xfd { printf("\t; OPCODE FD"); imprimir_direccion(1); }
\xff { printf("\tRST 38H"); imprimir_direccion(1); }

 /* instrucciones del prefijo CBh, todas de 3 bytes, fijos */

 \xcb\x00 { printf("\tRLC B"); imprimir_direccion(1); }
 \xcb\x01 { printf("\tRLC C"); imprimir_direccion(1); }
 \xcb\x02 { printf("\tRLC D"); imprimir_direccion(1); }
 \xcb\x03 { printf("\tRLC E"); imprimir_direccion(1); }
 \xcb\x04 { printf("\tRLC H"); imprimir_direccion(1); }
 \xcb\x05 { printf("\tRLC L"); imprimir_direccion(1); }
 \xcb\x06 { printf("\tRLC (HL)"); imprimir_direccion(1); }
 \xcb\x07 { printf("\tRLC A"); imprimir_direccion(1); }
 \xcb\x08 { printf("\tRRC B"); imprimir_direccion(1); }
 \xcb\x09 { printf("\tRRC C"); imprimir_direccion(1); }
 \xcb\x0a { printf("\tRRC D"); imprimir_direccion(1); }
 \xcb\x0b { printf("\tRRC E"); imprimir_direccion(1); }
 \xcb\x0c { printf("\tRRC H"); imprimir_direccion(1); }
 \xcb\x0d { printf("\tRRC L"); imprimir_direccion(1); }
 \xcb\x0e { printf("\tRRC (HL)"); imprimir_direccion(1); }
 \xcb\x0f { printf("\tRRC A"); imprimir_direccion(1); }
 
 \xcb\x10 { printf("\tRL B"); imprimir_direccion(1); }
 \xcb\x11 { printf("\tRL C"); imprimir_direccion(1); }
 \xcb\x12 { printf("\tRL D"); imprimir_direccion(1); }
 \xcb\x13 { printf("\tRL E"); imprimir_direccion(1); }
 \xcb\x14 { printf("\tRL H"); imprimir_direccion(1); }
 \xcb\x15 { printf("\tRL L"); imprimir_direccion(1); }
 \xcb\x16 { printf("\tRL (HL)"); imprimir_direccion(1); }
 \xcb\x17 { printf("\tRL A"); imprimir_direccion(1); }
 \xcb\x18 { printf("\tRR B"); imprimir_direccion(1); }
 \xcb\x19 { printf("\tRR C"); imprimir_direccion(1); }
 \xcb\x1a { printf("\tRR D"); imprimir_direccion(1); }
 \xcb\x1b { printf("\tRR E"); imprimir_direccion(1); }
 \xcb\x1c { printf("\tRR H"); imprimir_direccion(1); }
 \xcb\x1d { printf("\tRR L"); imprimir_direccion(1); }
 \xcb\x1e { printf("\tRR (HL)"); imprimir_direccion(1); }
 \xcb\x1f { printf("\tRR A"); imprimir_direccion(1); }
 
 \xcb\x20 { printf("\tSLA B"); imprimir_direccion(1); }
 \xcb\x21 { printf("\tSLA C"); imprimir_direccion(1); }
 \xcb\x22 { printf("\tSLA D"); imprimir_direccion(1); }
 \xcb\x23 { printf("\tSLA E"); imprimir_direccion(1); }
 \xcb\x24 { printf("\tSLA H"); imprimir_direccion(1); }
 \xcb\x25 { printf("\tSLA L"); imprimir_direccion(1); }
 \xcb\x26 { printf("\tSLA (HL)"); imprimir_direccion(1); }
 \xcb\x27 { printf("\tSLA A"); imprimir_direccion(1); }
 \xcb\x28 { printf("\tSRA B"); imprimir_direccion(1); }
 \xcb\x29 { printf("\tSRA C"); imprimir_direccion(1); }
 \xcb\x2a { printf("\tSRA D"); imprimir_direccion(1); }
 \xcb\x2b { printf("\tSRA E"); imprimir_direccion(1); }
 \xcb\x2c { printf("\tSRA H"); imprimir_direccion(1); }
 \xcb\x2d { printf("\tSRA L"); imprimir_direccion(1); }
 \xcb\x2e { printf("\tSRA (HL)"); imprimir_direccion(1); }
 \xcb\x2f { printf("\tSRA A"); imprimir_direccion(1); }
 
 \xcb\x30 { printf("\tSWAP B"); imprimir_direccion(1); }
 \xcb\x31 { printf("\tSWAP C"); imprimir_direccion(1); }
 \xcb\x32 { printf("\tSWAP D"); imprimir_direccion(1); }
 \xcb\x33 { printf("\tSWAP E"); imprimir_direccion(1); }
 \xcb\x34 { printf("\tSWAP H"); imprimir_direccion(1); }
 \xcb\x35 { printf("\tSWAP L"); imprimir_direccion(1); }
 \xcb\x36 { printf("\tSWAP (HL)"); imprimir_direccion(1); }
 \xcb\x37 { printf("\tSWAP A"); imprimir_direccion(1); }
 \xcb\x38 { printf("\tSRL B"); imprimir_direccion(1); }
 \xcb\x39 { printf("\tSRL C"); imprimir_direccion(1); }
 \xcb\x3a { printf("\tSRL D"); imprimir_direccion(1); }
 \xcb\x3b { printf("\tSRL E"); imprimir_direccion(1); }
 \xcb\x3c { printf("\tSRL H"); imprimir_direccion(1); }
 \xcb\x3d { printf("\tSRL L"); imprimir_direccion(1); }
 \xcb\x3e { printf("\tSRL (HL)"); imprimir_direccion(1); }
 \xcb\x3f { printf("\tSRL A"); imprimir_direccion(1); }
 
 \xcb\x40 { printf("\tBIT 0,B"); imprimir_direccion(1); }
 \xcb\x41 { printf("\tBIT 0,C"); imprimir_direccion(1); }
 \xcb\x42 { printf("\tBIT 0,D"); imprimir_direccion(1); }
 \xcb\x43 { printf("\tBIT 0,E"); imprimir_direccion(1); }
 \xcb\x44 { printf("\tBIT 0,H"); imprimir_direccion(1); }
 \xcb\x45 { printf("\tBIT 0,L"); imprimir_direccion(1); }
 \xcb\x46 { printf("\tBIT 0,(HL)"); imprimir_direccion(1); }
 \xcb\x47 { printf("\tBIT 0,A"); imprimir_direccion(1); }
 \xcb\x48 { printf("\tBIT 1,B"); imprimir_direccion(1); }
 \xcb\x49 { printf("\tBIT 1,C"); imprimir_direccion(1); }
 \xcb\x4a { printf("\tBIT 1,D"); imprimir_direccion(1); }
 \xcb\x4b { printf("\tBIT 1,E"); imprimir_direccion(1); }
 \xcb\x4c { printf("\tBIT 1,H"); imprimir_direccion(1); }
 \xcb\x4d { printf("\tBIT 1,L"); imprimir_direccion(1); }
 \xcb\x4e { printf("\tBIT 1,(HL)"); imprimir_direccion(1); }
 \xcb\x4f { printf("\tBIT 1,A"); imprimir_direccion(1); }
 
 \xcb\x50 { printf("\tBIT 2,B"); imprimir_direccion(1); }
 \xcb\x51 { printf("\tBIT 2,C"); imprimir_direccion(1); }
 \xcb\x52 { printf("\tBIT 2,D"); imprimir_direccion(1); }
 \xcb\x53 { printf("\tBIT 2,E"); imprimir_direccion(1); }
 \xcb\x54 { printf("\tBIT 2,H"); imprimir_direccion(1); }
 \xcb\x55 { printf("\tBIT 2,L"); imprimir_direccion(1); }
 \xcb\x56 { printf("\tBIT 2,(HL)"); imprimir_direccion(1); }
 \xcb\x57 { printf("\tBIT 2,A"); imprimir_direccion(1); }
 \xcb\x58 { printf("\tBIT 3,B"); imprimir_direccion(1); }
 \xcb\x59 { printf("\tBIT 3,C"); imprimir_direccion(1); }
 \xcb\x5a { printf("\tBIT 3,D"); imprimir_direccion(1); }
 \xcb\x5b { printf("\tBIT 3,E"); imprimir_direccion(1); }
 \xcb\x5c { printf("\tBIT 3,H"); imprimir_direccion(1); }
 \xcb\x5d { printf("\tBIT 3,L"); imprimir_direccion(1); }
 \xcb\x5e { printf("\tBIT 3,(HL)"); imprimir_direccion(1); }
 \xcb\x5f { printf("\tBIT 3,A"); imprimir_direccion(1); }
 
 \xcb\x60 { printf("\tBIT 4,B"); imprimir_direccion(1); }
 \xcb\x61 { printf("\tBIT 4,C"); imprimir_direccion(1); }
 \xcb\x62 { printf("\tBIT 4,D"); imprimir_direccion(1); }
 \xcb\x63 { printf("\tBIT 4,E"); imprimir_direccion(1); }
 \xcb\x64 { printf("\tBIT 4,H"); imprimir_direccion(1); }
 \xcb\x65 { printf("\tBIT 4,L"); imprimir_direccion(1); }
 \xcb\x66 { printf("\tBIT 4,(HL)"); imprimir_direccion(1); }
 \xcb\x67 { printf("\tBIT 4,A"); imprimir_direccion(1); }
 \xcb\x68 { printf("\tBIT 5,B"); imprimir_direccion(1); }
 \xcb\x69 { printf("\tBIT 5,C"); imprimir_direccion(1); }
 \xcb\x6a { printf("\tBIT 5,D"); imprimir_direccion(1); }
 \xcb\x6b { printf("\tBIT 5,E"); imprimir_direccion(1); }
 \xcb\x6c { printf("\tBIT 5,H"); imprimir_direccion(1); }
 \xcb\x6d { printf("\tBIT 5,L"); imprimir_direccion(1); }
 \xcb\x6e { printf("\tBIT 5,(HL)"); imprimir_direccion(1); }
 \xcb\x6f { printf("\tBIT 5,A"); imprimir_direccion(1); }
 
\xcb\x70 { printf("\tBIT 6,B"); imprimir_direccion(1); }
\xcb\x71 { printf("\tBIT 6,C"); imprimir_direccion(1); }
\xcb\x72 { printf("\tBIT 6,D"); imprimir_direccion(1); }
\xcb\x73 { printf("\tBIT 6,E"); imprimir_direccion(1); }
\xcb\x74 { printf("\tBIT 6,H"); imprimir_direccion(1); }
\xcb\x75 { printf("\tBIT 6,L"); imprimir_direccion(1); }
\xcb\x76 { printf("\tBIT 6,(HL)"); imprimir_direccion(1); }
\xcb\x77 { printf("\tBIT 6,A"); imprimir_direccion(1); }
\xcb\x78 { printf("\tBIT 7,B"); imprimir_direccion(1); }
\xcb\x79 { printf("\tBIT 7,C"); imprimir_direccion(1); }
\xcb\x7a { printf("\tBIT 7,D"); imprimir_direccion(1); }
\xcb\x7b { printf("\tBIT 7,E"); imprimir_direccion(1); }
\xcb\x7c { printf("\tBIT 7,H"); imprimir_direccion(1); }
\xcb\x7d { printf("\tBIT 7,L"); imprimir_direccion(1); }
\xcb\x7e { printf("\tBIT 7,(HL)"); imprimir_direccion(1); }
\xcb\x7f { printf("\tBIT 7,A"); imprimir_direccion(1); }
 
\xcb\x80 { printf("\tRES 0,B"); imprimir_direccion(1); }
\xcb\x81 { printf("\tRES 0,C"); imprimir_direccion(1); }
\xcb\x82 { printf("\tRES 0,D"); imprimir_direccion(1); }
\xcb\x83 { printf("\tRES 0,E"); imprimir_direccion(1); }
\xcb\x84 { printf("\tRES 0,H"); imprimir_direccion(1); }
\xcb\x85 { printf("\tRES 0,L"); imprimir_direccion(1); }
\xcb\x86 { printf("\tRES 0,(HL)"); imprimir_direccion(1); }
\xcb\x87 { printf("\tRES 0,A"); imprimir_direccion(1); }
\xcb\x88 { printf("\tRES 1,B"); imprimir_direccion(1); }
\xcb\x89 { printf("\tRES 1,C"); imprimir_direccion(1); }
\xcb\x8a { printf("\tRES 1,D"); imprimir_direccion(1); }
\xcb\x8b { printf("\tRES 1,E"); imprimir_direccion(1); }
\xcb\x8c { printf("\tRES 1,H"); imprimir_direccion(1); }
\xcb\x8d { printf("\tRES 1,L"); imprimir_direccion(1); }
\xcb\x8e { printf("\tRES 1,(HL)"); imprimir_direccion(1); }
\xcb\x8f { printf("\tRES 1,A"); imprimir_direccion(1); }
 
\xcb\x90 { printf("\tRES 2,B"); imprimir_direccion(1); }
\xcb\x91 { printf("\tRES 2,C"); imprimir_direccion(1); }
\xcb\x92 { printf("\tRES 2,D"); imprimir_direccion(1); }
\xcb\x93 { printf("\tRES 2,E"); imprimir_direccion(1); }
\xcb\x94 { printf("\tRES 2,H"); imprimir_direccion(1); }
\xcb\x95 { printf("\tRES 2,L"); imprimir_direccion(1); }
\xcb\x96 { printf("\tRES 2,(HL)"); imprimir_direccion(1); }
\xcb\x97 { printf("\tRES 2,A"); imprimir_direccion(1); }
\xcb\x98 { printf("\tRES 3,B"); imprimir_direccion(1); }
\xcb\x99 { printf("\tRES 3,C"); imprimir_direccion(1); }
\xcb\x9a { printf("\tRES 3,D"); imprimir_direccion(1); }
\xcb\x9b { printf("\tRES 3,E"); imprimir_direccion(1); }
\xcb\x9c { printf("\tRES 3,H"); imprimir_direccion(1); }
\xcb\x9d { printf("\tRES 3,L"); imprimir_direccion(1); }
\xcb\x9e { printf("\tRES 3,(HL)"); imprimir_direccion(1); }
\xcb\x9f { printf("\tRES 3,A"); imprimir_direccion(1); }
 
\xcb\xa0 { printf("\tRES 4,B"); imprimir_direccion(1); }
\xcb\xa1 { printf("\tRES 4,C"); imprimir_direccion(1); }
\xcb\xa2 { printf("\tRES 4,D"); imprimir_direccion(1); }
\xcb\xa3 { printf("\tRES 4,E"); imprimir_direccion(1); }
\xcb\xa4 { printf("\tRES 4,H"); imprimir_direccion(1); }
\xcb\xa5 { printf("\tRES 4,L"); imprimir_direccion(1); }
\xcb\xa6 { printf("\tRES 4,(HL)"); imprimir_direccion(1); }
\xcb\xa7 { printf("\tRES 4,A"); imprimir_direccion(1); }
\xcb\xa8 { printf("\tRES 5,B"); imprimir_direccion(1); }
\xcb\xa9 { printf("\tRES 5,C"); imprimir_direccion(1); }
\xcb\xaa { printf("\tRES 5,D"); imprimir_direccion(1); }
\xcb\xab { printf("\tRES 5,E"); imprimir_direccion(1); }
\xcb\xac { printf("\tRES 5,H"); imprimir_direccion(1); }
\xcb\xad { printf("\tRES 5,L"); imprimir_direccion(1); }
\xcb\xae { printf("\tRES 5,(HL)"); imprimir_direccion(1); }
\xcb\xaf { printf("\tRES 5,A"); imprimir_direccion(1); }
 
\xcb\xb0 { printf("\tRES 6,B"); imprimir_direccion(1); }
\xcb\xb1 { printf("\tRES 6,C"); imprimir_direccion(1); }
\xcb\xb2 { printf("\tRES 6,D"); imprimir_direccion(1); }
\xcb\xb3 { printf("\tRES 6,E"); imprimir_direccion(1); }
\xcb\xb4 { printf("\tRES 6,H"); imprimir_direccion(1); }
\xcb\xb5 { printf("\tRES 6,L"); imprimir_direccion(1); }
\xcb\xb6 { printf("\tRES 6,(HL)"); imprimir_direccion(1); }
\xcb\xb7 { printf("\tRES 6,A"); imprimir_direccion(1); }
\xcb\xb8 { printf("\tRES 7,B"); imprimir_direccion(1); }
\xcb\xb9 { printf("\tRES 7,C"); imprimir_direccion(1); }
\xcb\xba { printf("\tRES 7,D"); imprimir_direccion(1); }
\xcb\xbb { printf("\tRES 7,E"); imprimir_direccion(1); }
\xcb\xbc { printf("\tRES 7,H"); imprimir_direccion(1); }
\xcb\xbd { printf("\tRES 7,L"); imprimir_direccion(1); }
\xcb\xbe { printf("\tRES 7,(HL)"); imprimir_direccion(1); }
\xcb\xbf { printf("\tRES 7,A"); imprimir_direccion(1); }
 
\xcb\xc0 { printf("\tSET 0,B"); imprimir_direccion(1); }
\xcb\xc1 { printf("\tSET 0,C"); imprimir_direccion(1); }
\xcb\xc2 { printf("\tSET 0,D"); imprimir_direccion(1); }
\xcb\xc3 { printf("\tSET 0,E"); imprimir_direccion(1); }
\xcb\xc4 { printf("\tSET 0,H"); imprimir_direccion(1); }
\xcb\xc5 { printf("\tSET 0,L"); imprimir_direccion(1); }
\xcb\xc6 { printf("\tSET 0,(HL)"); imprimir_direccion(1); }
\xcb\xc7 { printf("\tSET 0,A"); imprimir_direccion(1); }
\xcb\xc8 { printf("\tSET 1,B"); imprimir_direccion(1); }
\xcb\xc9 { printf("\tSET 1,C"); imprimir_direccion(1); }
\xcb\xca { printf("\tSET 1,D"); imprimir_direccion(1); }
\xcb\xcb { printf("\tSET 1,E"); imprimir_direccion(1); }
\xcb\xcc { printf("\tSET 1,H"); imprimir_direccion(1); }
\xcb\xcd { printf("\tSET 1,L"); imprimir_direccion(1); }
\xcb\xce { printf("\tSET 1,(HL)"); imprimir_direccion(1); }
\xcb\xcf { printf("\tSET 1,A"); imprimir_direccion(1); }
 
\xcb\xd0 { printf("\tSET 2,B"); imprimir_direccion(1); }
\xcb\xd1 { printf("\tSET 2,C"); imprimir_direccion(1); }
\xcb\xd2 { printf("\tSET 2,D"); imprimir_direccion(1); }
\xcb\xd3 { printf("\tSET 2,E"); imprimir_direccion(1); }
\xcb\xd4 { printf("\tSET 2,H"); imprimir_direccion(1); }
\xcb\xd5 { printf("\tSET 2,L"); imprimir_direccion(1); }
\xcb\xd6 { printf("\tSET 2,(HL)"); imprimir_direccion(1); }
\xcb\xd7 { printf("\tSET 2,A"); imprimir_direccion(1); }
\xcb\xd8 { printf("\tSET 3,B"); imprimir_direccion(1); }
\xcb\xd9 { printf("\tSET 3,C"); imprimir_direccion(1); }
\xcb\xda { printf("\tSET 3,D"); imprimir_direccion(1); }
\xcb\xdb { printf("\tSET 3,E"); imprimir_direccion(1); }
\xcb\xdc { printf("\tSET 3,H"); imprimir_direccion(1); }
\xcb\xdd { printf("\tSET 3,L"); imprimir_direccion(1); }
\xcb\xde { printf("\tSET 3,(HL)"); imprimir_direccion(1); }
\xcb\xdf { printf("\tSET 3,A"); imprimir_direccion(1); }
 
\xcb\xe0 { printf("\tSET 4,B"); imprimir_direccion(1); }
\xcb\xe1 { printf("\tSET 4,C"); imprimir_direccion(1); }
\xcb\xe2 { printf("\tSET 4,D"); imprimir_direccion(1); }
\xcb\xe3 { printf("\tSET 4,E"); imprimir_direccion(1); }
\xcb\xe4 { printf("\tSET 4,H"); imprimir_direccion(1); }
\xcb\xe5 { printf("\tSET 4,L"); imprimir_direccion(1); }
\xcb\xe6 { printf("\tSET 4,(HL)"); imprimir_direccion(1); }
\xcb\xe7 { printf("\tSET 4,A"); imprimir_direccion(1); }
\xcb\xe8 { printf("\tSET 5,B"); imprimir_direccion(1); }
\xcb\xe9 { printf("\tSET 5,C"); imprimir_direccion(1); }
\xcb\xea { printf("\tSET 5,D"); imprimir_direccion(1); }
\xcb\xeb { printf("\tSET 5,E"); imprimir_direccion(1); }
\xcb\xec { printf("\tSET 5,H"); imprimir_direccion(1); }
\xcb\xed { printf("\tSET 5,L"); imprimir_direccion(1); }
\xcb\xee { printf("\tSET 5,(HL)"); imprimir_direccion(1); }
\xcb\xef { printf("\tSET 5,A"); imprimir_direccion(1); }
 
\xcb\xf0 { printf("\tSET 6,B"); imprimir_direccion(1); }
\xcb\xf1 { printf("\tSET 6,C"); imprimir_direccion(1); }
\xcb\xf2 { printf("\tSET 6,D"); imprimir_direccion(1); }
\xcb\xf3 { printf("\tSET 6,E"); imprimir_direccion(1); }
\xcb\xf4 { printf("\tSET 6,H"); imprimir_direccion(1); }
\xcb\xf5 { printf("\tSET 6,L"); imprimir_direccion(1); }
\xcb\xf6 { printf("\tSET 6,(HL)"); imprimir_direccion(1); }
\xcb\xf7 { printf("\tSET 6,A"); imprimir_direccion(1); }
\xcb\xf8 { printf("\tSET 7,B"); imprimir_direccion(1); }
\xcb\xf9 { printf("\tSET 7,C"); imprimir_direccion(1); }
\xcb\xfa { printf("\tSET 7,D"); imprimir_direccion(1); }
\xcb\xfb { printf("\tSET 7,E"); imprimir_direccion(1); }
\xcb\xfc { printf("\tSET 7,H"); imprimir_direccion(1); }
\xcb\xfd { printf("\tSET 7,L"); imprimir_direccion(1); }
\xcb\xfe { printf("\tSET 7,(HL)"); imprimir_direccion(1); }
\xcb\xff { printf("\tSET 7,A"); imprimir_direccion(1); }


\x10\x00 { printf("\tSTOP 0"); imprimir_direccion(1);  }

 /* instrucciones que requieren de un análisis más detallado */

\xe6{OCTETO} { printf("\tAND %02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\xe8{OCTETO} { printf("\tADD SP,%02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\xee{OCTETO} { printf("\tXOR %02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\xf0{OCTETO} { printf("\tLDH A,(%02Xh)",  (int)*(yytext+1)); imprimir_direccion(2); }
\xf6{OCTETO} { printf("\tOR %02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\xf8{OCTETO} { printf("\tLD HL,SP+%02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\xfe{OCTETO} { printf("\tCP %02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\x0e{OCTETO} { printf("\tLD C,%02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\x06{OCTETO} { printf("\tLD B,%02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\x16{OCTETO} { printf("\tLD D,%02Xh",  (int)*(yytext+1));  imprimir_direccion(2);}
\xc6{OCTETO} { printf("\tADD A,%02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\xce{OCTETO} { printf("\tADC A,%02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\xd6{OCTETO} { printf("\tSUB %02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\xde{OCTETO} { printf("\tSBC A,%02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\xe0{OCTETO} { printf("\tLDH (%02Xh),A",  (int)*(yytext+1)); imprimir_direccion(2); }
\x1e{OCTETO} { printf("\tLD E,%02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\x26{OCTETO} { printf("\tLD H,%02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\x2e{OCTETO} { printf("\tLD L,%02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }
\x36{OCTETO} { printf("\tLD (HL),%02Xh",  (int)*(yytext+1));  imprimir_direccion(2);}
\x3e{OCTETO} { printf("\tLD A,%02Xh",  (int)*(yytext+1)); imprimir_direccion(2); }

\x31{DIRECCION} { printf("\tLD SP,%04Xh", texto_a_direccion(yytext+1)); imprimir_direccion(3);}
\x21{DIRECCION} { printf("\tLD HL,%04Xh", texto_a_direccion(yytext+1)); imprimir_direccion(3);}
\x01{DIRECCION} { printf("\tLD BC,%04Xh", texto_a_direccion(yytext+1)); imprimir_direccion(3);}
\x11{DIRECCION} { printf("\tLD DE,%04Xh,", texto_a_direccion(yytext+1)); imprimir_direccion(3);}
\xfa{DIRECCION} { printf("\tLD A,(%04Xh)", texto_a_direccion(yytext+1)); imprimir_direccion(3);}
\xea{DIRECCION} { printf("\tLD (%04Xh),A", texto_a_direccion(yytext+1)); imprimir_direccion(3);}
\x08{DIRECCION} { printf("\tLD (%04Xh),SP", texto_a_direccion(yytext+1)); imprimir_direccion(3);}

 /* saltos  alteran la instrucción que se ejecuta a continuación */
\x18{OCTETO} { printf("\tJR %02Xh",  (unsigned int)*(yytext+1)); imprimir_direccion(2); }
\x20{OCTETO} { printf("\tJR NZ,%02Xh",  (unsigned int)*(yytext+1));  imprimir_direccion(2);}
\x28{OCTETO} { printf("\tJR Z,%02Xh",  (unsigned int)*(yytext+1)); imprimir_direccion(2); }
\x30{OCTETO} { printf("\tJR NC,%02Xh",  (unsigned int)*(yytext+1));  imprimir_direccion(2);}
\x38{OCTETO} { printf("\tJR C,%02Xh",  (unsigned int)*(yytext+1)); imprimir_direccion(2); }

\xc2{DIRECCION} { printf("\tJP NZ,%04Xh",texto_a_direccion(yytext+1)); imprimir_direccion(3);}
\xc3{DIRECCION} { printf("\tJP %04Xh", texto_a_direccion( yytext+1 ) ); imprimir_direccion(3); }
\xc4{DIRECCION} { printf("\tCALL NZ,%04Xh", texto_a_direccion(yytext+1));imprimir_direccion(3);}
\xca{DIRECCION} { printf("\tJP Z,%04Xh", texto_a_direccion(yytext+1)); imprimir_direccion(3);}
\xcc{DIRECCION} { printf("\tCALL Z,%04Xh", texto_a_direccion(yytext+1)); imprimir_direccion(3);}
\xcd{DIRECCION} { printf("\tCALL %04Xh", texto_a_direccion(yytext+1)); imprimir_direccion(3);}
\xd2{DIRECCION} { printf("\tJP NC,%04Xh", texto_a_direccion(yytext+1)); imprimir_direccion(3);}
\xd4{DIRECCION} { printf("\tCALL NC,%04Xh",texto_a_direccion(yytext+1));imprimir_direccion(3);}
\xda{DIRECCION} { printf("\tJP C,%04Xh", texto_a_direccion(yytext+1)); imprimir_direccion(3);}
\xdc{DIRECCION} { printf("\tCALL C,%04Xh", texto_a_direccion(yytext+1)); imprimir_direccion(3);}

. { printf("esto no pasa NUNCA"); imprimir_direccion(1); }
%% 

int main(int n, char **args)
{
  FILE *fp = NULL;

  if( n > 1 ) 
  {
    fp = fopen(*(args+1),"r");
    if(fp!=NULL)
    {
      yyin = fp; 
    }
  }

  yylex();

  if(fp!=NULL)
  {
    fclose( fp );
  }
  return 0;
}
